@extends('fe.components.main')
@section('content')
    @include('fe.components.breadcrumbs', ['title' => $title])

    <div class="w-100 py-4">
        <div class="container">
            <div class="row">
                <div class="col-md-8 mb-3">
                    <div class="mb-4">
                        <h5 class="fw-bold mb-3 text-uppercase" style="color: #0A4C7F;">{{ $title }}</h5>
                        <div class="rounded" style="background-color: #E9974B;width:80px;height:6px;"></div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-4">
                            <span class="grey">Pelindung</span>
                        </div>
                        <div class="col-8 ">
                            <p class="mb-0 fw-bold">Dr. Supari, S.T.,M.T.</p>
                            <span class="fw-light">Rektor Universitas Semarang</span>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-4">
                            <span class="grey">Dewan Pembina</span>
                        </div>
                        <div class="col-8 ">
                            <p class="mb-0 fw-bold">Dr. Muhammad Junaidi, S.HI.,M.H.</p>
                            <span class="fw-light">Wakil Rektor III</span>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-4">
                            <span class="grey">Dewan Penasehat</span>
                        </div>
                        <div class="col-8 ">
                            <ul class="fw-bold ps-3 mb-0">
                                <li>Brigjen Pol. Agus Salim, SH, MH</li>
                                <li>Ade Ardiyansyah, ST</li>
                            </ul>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-4">
                            <span class="grey">Ketua Umum</span>
                        </div>
                        <div class="col-8 ">
                            <p class="mb-0 fw-bold">Ir. Yan Arianto, MM</p>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-4">
                            <span class="grey">Wakil Ketua</span>
                        </div>
                        <div class="col-8 ">
                            <p class="mb-0 fw-bold">Budi Prayitno, SH, MH</p>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-4">
                            <span class="grey">Sekjen</span>
                        </div>
                        <div class="col-8 ">
                            <p class="mb-0 fw-bold">Rully Tyas Fitriani, ST</p>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-4">
                            <span class="grey">Wakil Sekjen</span>
                        </div>
                        <div class="col-8 ">
                            <p class="mb-0 fw-bold">A. Dwi Nuryanto, SH, MH</p>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-4">
                            <span class="grey">Bendahara</span>
                        </div>
                        <div class="col-8 ">
                            <p class="mb-0 fw-bold">Khoirul Anwar, SH, MH</p>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-4">
                            <span class="grey">Wakil Bendahara</span>
                        </div>
                        <div class="col-8 ">
                            <p class="mb-0 fw-bold">M. Trijoko, ST, MM</p>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-4">
                            <span class="grey">Koordinator Bidang</span>
                        </div>
                        <div class="col-8 ">
                            <p class="mb-0 fw-bold">Teguh Triyanto, ST</p>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-4">
                            <span class="grey">Direktur Bidang 1 (Network & Communication)</span>
                        </div>
                        <div class="col-8 ">
                            <p class="mb-0 fw-bold">Sucipto, ST, MM</p>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-4">
                            <span class="grey">Anggota Bidang 1</span>
                        </div>
                        <div class="col-8 ">
                            <ul class="fw-bold ps-3">
                                <li>Fatur Rokhman, Spsi, MKom</li>
                                <li>Ricky Putra Mahardika, ST</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="mb-5">
                        <h5 class="fw-bold mb-3 text-uppercase" style="color: #0A4C7F;">Postingan Loker</h5>
                        <div class="rounded" style="background-color: #E9974B;width:80px;height:6px;"></div>
                        <div class="mt-3">
                            <div class="row mb-3 border-bottom pb-3">
                                <div class="col-4">
                                    <img src="{{ asset('img/loker/pameo.png') }}" alt="" class="w-100">
                                </div>
                                <div class="col-8">
                                    <p class="fw-semibold fs-6">PT Nihon Seiki Indonesia
                                    </p>
                                    <span class="fw-light"><i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;23 September
                                        2022</span>
                                </div>
                            </div>
                            <div class="row mb-3 border-bottom pb-3">
                                <div class="col-4">
                                    <img src="{{ asset('img/loker/pameo.png') }}" alt="" class="w-100">
                                </div>
                                <div class="col-8">
                                    <p class="fw-semibold fs-6">PT Nihon Seiki Indonesia
                                    </p>
                                    <span class="fw-light"><i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;23 September
                                        2022</span>
                                </div>
                            </div>
                            <div class="row mb-3 border-bottom pb-3">
                                <div class="col-4">
                                    <img src="{{ asset('img/loker/pameo.png') }}" alt="" class="w-100">
                                </div>
                                <div class="col-8">
                                    <p class="fw-semibold fs-6">PT Nihon Seiki Indonesia
                                    </p>
                                    <span class="fw-light"><i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;23 September
                                        2022</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mb-4">
                        <h5 class="fw-bold mb-3 text-uppercase" style="color: #0A4C7F;">Postingan Berita</h5>
                        <div class="rounded" style="background-color: #E9974B;width:80px;height:6px;"></div>
                        <div class="mt-3">
                            <div class="row mb-3 border-bottom pb-3">
                                <div class="col-4">
                                    <div class="w-100 h-100 bg-center"
                                        style="background-image:url({{ asset('img/content/usm1.jpg') }})"></div>
                                </div>
                                <div class="col-8">
                                    <p class="fw-semibold fs-6">IKA USM Memiliki Kepengurusan Baru dari Munas
                                    </p>
                                    <span class="fw-light"><i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;23 September
                                        2022</span>
                                </div>
                            </div>
                            <div class="row mb-3 border-bottom pb-3">
                                <div class="col-4">
                                    <div class="w-100 h-100 bg-center"
                                        style="background-image:url({{ asset('img/content/berita-22.png') }})"></div>
                                </div>
                                <div class="col-8">
                                    <p class="fw-semibold fs-6">Virtual Campus Hiring PT Porto Indonesia Sejahtera dengan
                                        Universitas Semarang
                                    </p>
                                    <span class="fw-light"><i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;23 September
                                        2022</span>
                                </div>
                            </div>
                            <div class="row mb-3 border-bottom pb-3">
                                <div class="col-4">
                                    <div class="w-100 h-100 bg-center"
                                        style="background-image:url({{ asset('img/content/usm1.jpg') }})"></div>
                                </div>
                                <div class="col-8">
                                    <p class="fw-semibold fs-6">Universitas Semarang mengadakan Seminar Internasional
                                        Tracer
                                        Study
                                    </p>
                                    <span class="fw-light"><i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;23 September
                                        2022</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <style>
        .bg-center {
            background-position: center;
            background-size: cover;
        }

        .grey {
            color: #73737B;
        }
    </style>
@endsection
