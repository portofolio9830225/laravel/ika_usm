<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">

    <title>IKA USM</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css'
        integrity='sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A=='
        crossorigin='anonymous' />
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/logo-usm.png') }}">
    <style>
        .bg-primary-1 {
            background-color: #073C64;
        }

        .cursor-pointer {
            cursor: pointer;
        }

        .text-sub-color {
            color: #9DB8CD;
        }

        .txt-underline {
            text-decoration: none;
        }

        .txt-underline:hover {
            text-decoration: underline;
        }
    </style>
    @yield('css')
</head>

<body>

    @include('fe.components.navbar')

    @yield('content')

    @include('fe.components.footer')

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/typed.js@2.0.12"></script>
    @yield('js')
    <script>
        function myFunction(x) {
            if (x.matches) { // If media query matches
                document.getElementById('menu-toggle').classList.add('d-none');
            } else {
                document.getElementById('menu-toggle').classList.remove('d-none');
            }
        }

        var x = window.matchMedia("(min-width: 992px)")
        myFunction(x) // Call listener function at run time
        x.addListener(myFunction) // Attach listener function on state change

        new Typed('.typed-1', {
            // Waits 1000ms after typing "First"
            strings: ['IKA USM', 'Ikatan Alumni', 'Universitas Semarang'],
            smartBackspace: true,
            typeSpeed: 50,
            backSpeed: 50,
            loop: true,
        });
    </script>
</body>

</html>
