<section class="w-100 py-3" style="background-color: #073455;" class="top">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <p class="mb-0 text-white fw-light">Selamat Datang di <span class="fw-bold typed-1"></span></p>
            </div>
        </div>
    </div>
</section>

<!-- Navbar -->
<nav class="navbar navbar-expand-lg" style="background-image:linear-gradient(#073C64, #0B5C99) ;">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ asset('img/logo.png') }}" alt="Logo USM"
                class="d-inline-block align-text-top"></a>
        <div class="fs-3 text-white cursor-pointer" id="menu-toggle" data-bs-toggle="offcanvas"
            data-bs-target="#offcanvasExample" aria-controls="offcanvasExample"><i class="fa-solid fa-bars"></i></div>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav ms-auto">
                <li class="nav-item">
                    <a class="nav-link text-white" href="{{ url('/') }}">Beranda</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link text-white dropdown-toggle" href="javascript:;" role="button"
                        data-bs-toggle="dropdown" aria-expanded="false">
                        Tentang Kami
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="{{ url('struktur-organisasi') }}">Struktur Organisasi</a>
                        </li>
                        <li><a class="dropdown-item" href="{{ url('profile') }}">Profile</a></li>
                        <li><a class="dropdown-item" href="{{ url('kegiatan-ika') }}">Kegiatan IKA</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link text-white dropdown-toggle" href="#" role="button"
                        data-bs-toggle="dropdown" aria-expanded="false">
                        Alumni
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="{{ url('kartu-tanda-alumni') }}">Kartu Tanda Alumni</a></li>
                        <li><a class="dropdown-item" href="#">Legalisir Ijazah</a></li>
                        <li><a class="dropdown-item" href="https://tracerstudy.usm.ac.id/" target="_blank">Tracer
                                Study</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link text-white dropdown-toggle" href="#" role="button"
                        data-bs-toggle="dropdown" aria-expanded="false">
                        USM Karier
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="{{ url('lowongan-kerja') }}">Lowongan Kerja</a></li>
                        <li><a class="dropdown-item" href="{{ url('lowongan-magang') }}">Magang</a></li>
                        <li><a class="dropdown-item" href="{{ url('tips-karier') }}">Tips Karier</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link text-white dropdown-toggle" href="#" role="button"
                        data-bs-toggle="dropdown" aria-expanded="false">
                        Profile Alumni
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="{{ url('alumni-unggul') }}">Alumni Unggul</a></li>
                        <li><a class="dropdown-item" href="{{ url('news-alumni') }}">News Alumni</a></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white" href="{{ url('contact-us') }}">Kontak Kami</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="offcanvas offcanvas-start" style="background-color: #073455;" tabindex="-1" id="offcanvasExample"
    aria-labelledby="offcanvasExampleLabel">
    <div class="offcanvas-header">
        <h5 class="offcanvas-title text-white" id="offcanvasExampleLabel">Menu</h5>
        <div class="fs-4 text-white cursor-pointer" data-bs-dismiss="offcanvas"><i
                class="fa-sharp fa-solid fa-circle-xmark"></i></div>
    </div>
    <div class="offcanvas-body ">

        <div class="mb-3">
            <a href="{{ url('/') }}" class="fs-5 text-white txt-underline fw-normal ">Beranda</a>
        </div>

        <div class="dropdown mb-3">
            <a href="javascript:;" class="fs-5 text-white txt-underline fw-normal dropdown-toggle"
                data-bs-toggle="dropdown">Tentang Kami</a>
            <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="{{ url('struktur-organisasi') }}">Struktur Organisasi</a></li>
                <li><a class="dropdown-item" href="{{ url('profile') }}">Profile</a>
                <li><a class="dropdown-item" href="{{ url('kegiatan-ika') }}">Kegiatan IKA</a>
                </li>
            </ul>
        </div>

        <div class="dropdown mb-3">
            <a href="javascript:;" class="fs-5 text-white txt-underline fw-normal dropdown-toggle"
                data-bs-toggle="dropdown">Alumni</a>
            <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="{{ url('kartu-tanda-alumni') }}">Kartu Tanda Alumni</a></li>
                <li><a class="dropdown-item" href="#">Legalisir Ijazah</a></li>
                <li><a class="dropdown-item" href="https://tracerstudy.usm.ac.id/">Tracer Study</a></li>
            </ul>
        </div>

        <div class="dropdown mb-3">
            <a href="javascript:;" class="fs-5 text-white txt-underline fw-normal dropdown-toggle"
                data-bs-toggle="dropdown">USM Karier</a>
            <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="{{ url('lowongan-kerja') }}">Lowongan Kerja</a></li>
                <li><a class="dropdown-item" href="{{ url('lowongan-magang') }}">Magang</a></li>
                <li><a class="dropdown-item" href="{{ url('tips-karier') }}">Tips Karier</a></li>
            </ul>
        </div>

        <div class="dropdown mb-3">
            <a href="javascript:;" class="fs-5 text-white txt-underline fw-normal dropdown-toggle"
                data-bs-toggle="dropdown">Profile Alumni</a>
            <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="{{ url('alumni-awards') }}">Alumni Unggul</a></li>
                <li><a class="dropdown-item" href="{{ url('news-alumni') }}">News Alumni</a></li>
            </ul>
        </div>

        <div class="mb-3">
            <a href="{{ url('contact-us') }}" class="fs-5 text-white txt-underline fw-normal ">Kontak Kami</a>
        </div>

    </div>
</div>
