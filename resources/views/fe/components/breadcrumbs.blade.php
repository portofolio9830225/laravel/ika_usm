<div class="w-100 py-4">
    <div class="container">
        <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#" class="text-decoration-none text-secondary">Beranda</a></li>
                @if (isset($list))
                    <li class="breadcrumb-item"><a href="#"
                            class="text-decoration-none text-secondary">{{ $list_nama }}</a>
                    </li>
                    <li class="breadcrumb-item fw-semibold" style="color:#0A4C7F;">{{ $title }}</li>
                @else
                    <li class="breadcrumb-item fw-semibold" style="color:#0A4C7F;">{{ $title }}</li>
                @endif
            </ol>
        </nav>
    </div>
</div>
