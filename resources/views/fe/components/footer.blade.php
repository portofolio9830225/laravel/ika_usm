<section class="w-100 py-5" style="background-image:linear-gradient(#073C64, #0B5C99) ;">
    <div class="container">
        <div class="row">
            <div class="col-md-3 mb-3">
                <img src="{{ asset('img/logo.png') }}" class="img-fluid mb-3">
                <p class="mb-0 text-sub-color">2F92+4VW, Jl. Soekarno Hatta, RT.7/RW.7, Tlogosari Kulon, Kec.
                    Pedurungan,
                    Kota Semarang, Jawa Tengah
                    59160, Indonesia</p>
            </div>
            <div class="col-md-3 mb-3">
                <p class="text-uppercase fw-semibold text-white">Tentang Kami</p>
                <p class="text-sub-color">Berita</p>
                <p class="text-sub-color">Alumni</p>
                <p class="text-sub-color">Lowongan</p>
                <p class="text-sub-color">Tracer Study</p>
                <p class="text-sub-color">Kontak Kami</p>
            </div>
            <div class="col-md-3 mb-3">
                <p class="text-uppercase fw-semibold text-white">Tautan Terkait</p>
                <p class="text-sub-color">Tentang USM</p>
                <p class="text-sub-color">Fakultas</p>
                <p class="text-sub-color">Perpustakaan</p>
            </div>
            <div class="col-md-3 mb-3">
                <p class="text-uppercase fw-semibold text-white">Social Media Kami</p>
                <div class="text-white">
                    <i class="fa-brands fa-facebook fs-4 me-3"></i><i class="fa-brands fa-instagram fs-4 me-3"></i><i
                        class="fa-brands fa-youtube fs-4"></i>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="w-100 py-3" style="background-color: #073455;">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <p class="mb-0 text-white">IKA Universitas Semarang</p>
            </div>
            <div class="col-md-6 text-md-end">
                <p class="mb-0 text-white">Copyright © {{ date('Y') }}. All rights reserved</p>
            </div>
        </div>
    </div>
</section>
