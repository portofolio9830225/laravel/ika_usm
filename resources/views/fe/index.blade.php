@extends('fe.components.main')
@section('content')
    <section class="w-100 pt-5">
        <div class="container">
            <div class="swiper mySwiper">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img src="{{ asset('img/slider/banner-711.png') }}" alt="" class="img-fluid">
                    </div>
                    <div class="swiper-slide">
                        <img src="{{ asset('img/slider/banner-721.png') }}" alt="" class="img-fluid">
                    </div>
                    <div class="swiper-slide">
                        <img src="{{ asset('img/slider/banner-731.png') }}" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="w-100 py-5">
        <div class="container">
            <div class="row mb-3">
                <div class="col-md-6 mb-4" data-aos="fade-right" data-aos-duration="1000">
                    <h5 class="fw-bold mb-3" style="color: #0A4C7F;">TEMUKAN BERITA SEPUTAR ALUMNI USM</h5>
                    <div class="rounded" style="background-color: #E9974B;width:80px;height:6px;"></div>
                </div>
                <div class="col-md-6 text-end" data-aos="fade-up" data-aos-duration="1000">
                    <button class="btn-primary-1 p-3" onclick="window.location='{{ url('career-class') }}'">Baca
                        Selengkapnya</button>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 mb-3" data-aos="fade-up" data-aos-duration="1000">
                    <img src="{{ asset('img/content/usm1.jpg') }}" alt="" class="w-100 mb-3 h-250"
                        style="object-fit: cover;object-position: center;">
                    <div class="mb-3">
                        <div class="w-100 d-flex">
                            <div class="d-flex flex-column align-items-center border-end me-2" style="width: 20%;">
                                <span class="fw-bold fs-5">01</span>
                                <span>Sept</span>
                            </div>
                            <div class="fw-semibold" style="width: 80%;">
                                IKA USM Memiliki Kepengurusan Baru dari Munas
                            </div>
                        </div>
                    </div>
                    <div class="pb-4 border-bottom">
                        <p class="mb-0 fw-light" style="text-align: justify;">Semarang – Dalam dunia Pendidikan, tak
                            selalu
                            hanya input yang dipikirkan, tetapi juga outputnya.
                            Karena itu alumni menjadi hal yang sangat penting juga di dalam dunia tersebut.
                            Begitu pula Universitas Semarang (USM) yang merupakan salah satu kampus swasta terbesar di
                            Semarang,</p>
                    </div>
                </div>
                <div class="col-md-4 mb-3" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="400">
                    <img src="{{ asset('img/content/berita-22.png') }}" alt="" class="w-100 mb-3 h-250">
                    <div class="mb-3">
                        <div class="w-100 d-flex">
                            <div class="d-flex flex-column align-items-center border-end me-2" style="width: 20%;">
                                <span class="fw-bold fs-5">01</span>
                                <span>Sept</span>
                            </div>
                            <div class="fw-semibold" style="width: 80%;">
                                Virtual Campus Hiring PT Porto Indonesia Sejahtera dengan Universitas Semarang
                            </div>
                        </div>
                    </div>
                    <div class="pb-4 border-bottom">
                        <p class="mb-0 fw-light" style="text-align: justify;">
                            Nama besar universitas di dunia, salah satunya karena faktor hubungan yang kuat dengan
                            alumninya. Hal itu diungkapkan Kairul Anwar yang terpilih menjadi Bendahara Umum Ikatan Alumni
                            Universitas Semarang (IKA USM).</p>
                    </div>
                </div>
                <div class="col-md-4 mb-3" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="800">
                    <img src="{{ asset('img/content/usm1.jpg') }}" alt="" class="w-100 mb-3 h-250"
                        style="object-fit: cover;object-position: center;">
                    <div class="mb-3">
                        <div class="w-100 d-flex">
                            <div class="d-flex flex-column align-items-center border-end me-2" style="width: 20%;">
                                <span class="fw-bold fs-5">01</span>
                                <span>Sept</span>
                            </div>
                            <div class="fw-semibold" style="width: 80%;">
                                Universitas Semarang mengadakan Seminar Internasional Tracer Study
                            </div>
                        </div>
                    </div>
                    <div class="pb-4 border-bottom">
                        <p class="mb-0 fw-light" style="text-align: justify;">Dalam rangka pelaksanaan program Tracer
                            Study
                            serta bantuan program fasilitasi pelaksanaan Tracer Study tahun 2021, Kementrian Pendidikan,
                            Kebudayaan, Riset, Dan Teknologi melaksanakan Seminar Internasional ...</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="w-100 py-5 bg-ukuran" style="background-image: url({{ asset('img/loker3.png') }})">
        <div class="container bg-white p-4 shadow">
            <div class="row">
                <div class="col-md-6 mb-5">
                    <div class="mb-4" data-aos="fade-right" data-aos-duration="1000">
                        <h5 class="fw-bold mb-3" style="color: #0A4C7F;">TEMUKAN LOWONGAN KERJA</h5>
                        <div class="rounded" style="background-color: #E9974B;width:80px;height:6px;"></div>
                    </div>
                    <div class="row mb-4 border-bottom pb-4" data-aos="fade-right" data-aos-duration="1000"
                        data-aos-delay="400">
                        <div class="col-4">
                            <img src="{{ asset('img/loker/pameo.png') }}" alt="" class="w-100 h-108">
                        </div>
                        <div class="col-8">
                            <p class="fw-semibold fs-6">PT Nihon Seiki Indonesia
                            </p>
                            <span class="fw-light"><i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;23 September 2022</span>
                        </div>
                    </div>
                    <div class="row mb-4 border-bottom pb-4" data-aos="fade-right" data-aos-duration="1000"
                        data-aos-delay="600">
                        <div class="col-4">
                            <img src="{{ asset('img/loker/myrobin.png') }}" alt="" class="w-100 h-108">
                        </div>
                        <div class="col-8">
                            <p class="fw-semibold fs-6">PT Myrobin Indonesia Teknologi
                            </p>
                            <span class="fw-light"><i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;23 September 2022</span>
                        </div>
                    </div>
                    <div class="row mb-3" data-aos="fade-right" data-aos-duration="1000" data-aos-delay="800">
                        <div class="col-4">
                            <img src="{{ asset('img/loker/allianz.png') }}" alt="" class="w-100 h-108">
                        </div>
                        <div class="col-8">
                            <p class="fw-semibold fs-6">
                                Allianz Indonesia
                            </p>
                            <span class="fw-light"><i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;23 September 2022</span>
                        </div>
                    </div>
                    <div class="w-100 d-flex justify-content-end" data-aos="fade-up" data-aos-duration="800"
                        data-aos-anchor-placement="top-bottom">
                        <button class="btn-primary-1 p-3" onclick="window.location='{{ url('lowongan-magang') }}'">Baca
                            Selengkapnya</button>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-4" data-aos="fade-right" data-aos-duration="1000">
                        <h5 class="fw-bold mb-3" style="color: #0A4C7F;">TEMUKAN LOWONGAN MAGANG</h5>
                        <div class="rounded" style="background-color: #E9974B;width:80px;height:6px;"></div>
                    </div>
                    <div class="row mb-4 border-bottom pb-4" data-aos="fade-right" data-aos-duration="1000"
                        data-aos-delay="400">
                        <div class="col-4">
                            <img src="{{ asset('img/loker/freeport.jpg') }}" alt="" class="w-100 h-108">
                        </div>
                        <div class="col-8">
                            <p class="fw-semibold fs-6">PT Freeport Indonesia
                            </p>
                            <span class="fw-light"><i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;23 September 2022</span>
                        </div>
                    </div>
                    <div class="row mb-4 border-bottom pb-4" data-aos="fade-right" data-aos-duration="1000"
                        data-aos-delay="600">
                        <div class="col-4">
                            <img src="{{ asset('img/loker/jebsen.png') }}" alt="" class="w-100 h-108">
                        </div>
                        <div class="col-8">
                            <p class="fw-semibold fs-6">PT Jebsen & Jessen Technology Indonesia
                            </p>
                            <span class="fw-light"><i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;23 September 2022</span>
                        </div>
                    </div>
                    <div class="row mb-3" data-aos="fade-right" data-aos-duration="1000" data-aos-delay="800">
                        <div class="col-4">
                            <img src="{{ asset('img/loker/horizon.png') }}" alt="" class="w-100 h-108">
                        </div>
                        <div class="col-8">
                            <p class="fw-semibold fs-6">
                                PT HZN Teknologi Indonesia
                            </p>
                            <span class="fw-light"><i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;23 September 2022</span>
                        </div>
                    </div>
                    <div class="w-100 d-flex justify-content-end" data-aos="fade-up" data-aos-duration="800"
                        data-aos-anchor-placement="top-bottom">
                        <button class="btn-primary-1 p-3" onclick="window.location='{{ url('lowongan-magang') }}'">Baca
                            Selengkapnya</button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="w-100 py-5">
        <div class="container">
            <div class="row mb-3">
                <div class="col-md-6 mb-4" data-aos="fade-right" data-aos-duration="1000">
                    <h5 class="fw-bold mb-3" style="color: #0A4C7F;">TEMUKAN TIPS BERKARIER</h5>
                    <div class="rounded" style="background-color: #E9974B;width:80px;height:6px;"></div>
                </div>
                <div class="col-md-6 text-end" data-aos="fade-up" data-aos-duration="1000">
                    <button class="btn-primary-1 p-3">Baca Selengkapnya</button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 mb-3" data-aos="fade-up" data-aos-duration="1000">
                    <img src="{{ asset('img/karir/tips-1.png') }}" alt="" class="w-100 mb-3 h-250">
                    <div class="mb-3">
                        <div class="w-100 d-flex">
                            <div class="d-flex flex-column align-items-center border-end me-2" style="width: 20%;">
                                <span class="fw-bold fs-5">01</span>
                                <span>Sept</span>
                            </div>
                            <div class="fw-semibold" style="width: 80%;">
                                Cara Membuat CV dan Cover Letter yang Bagus
                            </div>
                        </div>
                    </div>
                    <div class="pb-4 border-bottom">
                        <p class="mb-0 fw-light" style="text-align: justify;">Curriculum Vitae adalah kunci penting ketika
                            ingin bergabung ke suatu perusahaan. Saat proses rekuritmen calon pegawai dilakukan, perusahaan
                            selalu minta para pelamar untuk menyertakan CV. Fungsinya, menjadi panduan awal penyeleksi ...
                        </p>
                    </div>
                </div>
                <div class="col-md-4 mb-3" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="400">
                    <img src="{{ asset('img/karir/tips-2.png') }}" alt="" class="w-100 mb-3 h-250">
                    <div class="mb-3">
                        <div class="w-100 d-flex">
                            <div class="d-flex flex-column align-items-center border-end me-2" style="width: 20%;">
                                <span class="fw-bold fs-5">01</span>
                                <span>Sept</span>
                            </div>
                            <div class="fw-semibold" style="width: 80%;">
                                Lima Tips Menciptakan Citra Online yang Positif di Mata Perekrut
                            </div>
                        </div>
                    </div>
                    <div class="pb-4 border-bottom">
                        <p class="mb-0 fw-light" style="text-align: justify;">Seiring dengan perkembangan teknologi
                            informasi yang pesat, sistem perekrutan pegawai pun tak ketinggalan memanfaatkan media sosial
                            untuk memperoleh calon-calon pegawai yang potensial. Curriculum Vitae dan Surat Lamaran tetap
                            menjadi syarat ...
                        </p>
                    </div>
                </div>
                <div class="col-md-4 mb-3" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="800">
                    <img src="{{ asset('img/karir/tips-3.png') }}" alt="" class="w-100 mb-3 h-250">
                    <div class="mb-3">
                        <div class="w-100 d-flex">
                            <div class="d-flex flex-column align-items-center border-end me-2" style="width: 20%;">
                                <span class="fw-bold fs-5">01</span>
                                <span>Sept</span>
                            </div>
                            <div class="fw-semibold" style="width: 80%;">
                                Tips Mengelola Jadwal Cuti Kerja
                            </div>
                        </div>
                    </div>
                    <div class="pb-4 border-bottom">
                        <p class="mb-0 fw-light" style="text-align: justify;">Curriculum Vitae adalah kunci penting ketika
                            ingin bergabung ke suatu perusahaan. Saat proses rekuritmen calon pegawai dilakukan, perusahaan
                            selalu minta para pelamar untuk menyertakan CV. Fungsinya, menjadi panduan awal penyeleksi ...
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="w-100 py-5 bg-ukuran-center overflow-hidden"
        style="background-image: url({{ asset('img/bg-webusm-1.png') }});background-attachment: fixed;">
        <div class="container">
            <div class="mb-4" data-aos="fade-down" data-aos-duration="1000">
                <h5 class="fw-bold mb-3 text-white">VIDEO & PROFILE ALUMNI UNGGUL</h5>
                <div class="rounded" style="background-color: #E9974B;width:80px;height:6px;"></div>
            </div>
            <div class="row">
                <div class="col-md-6" data-aos="fade-right" data-aos-duration="1000">
                </div>
                <div class="col-md-6" data-aos="fade-left" data-aos-duration="1000">
                    <img src="{{ asset('img/profile3.png') }}" class="img-fluid">
                </div>
            </div>
        </div>
    </section>

    <section class="w-100 py-5">
        <div class="container">
            <div class="row mb-3">
                <div class="col-md-6" data-aos="fade-right" data-aos-duration="1000">
                    <h5 class="fw-bold mb-4" style="color: #0A4C7F;">MAJALAH KABAR ALUMNI</h5>
                    <div class="rounded" style="background-color: #E9974B;width:80px;height:6px;"></div>
                </div>
                <div class="col-md-6 text-end" data-aos="fade-up" data-aos-duration="1000">
                    <button class="btn-primary-1 p-3">Baca Selengkapnya</button>
                </div>
            </div>
            <div class="row pb-4 border-bottom">
                <div class="col-md-4" data-aos="fade-up" data-aos-duration="1000">
                    <img src="{{ asset('img/majalah1.png') }}" alt="" class="w-100 mb-3 h-250">
                    <div class="mb-3">
                        <div class="w-100 d-flex">
                            <div class="d-flex flex-column align-items-center border-end me-2" style="width: 20%;">
                                <span class="fw-light fs-6">Page</span>
                                <span class="fw-semibold fs-4">35</span>
                            </div>
                            <div class="fw-semibold" style="width: 80%;">
                                Ibu Dosen USM Gelar Bakti Sosial
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="400">
                    <img src="{{ asset('img/majalah2.png') }}" alt="" class="w-100 mb-3 h-250">
                    <div class="mb-3">
                        <div class="w-100 d-flex">
                            <div class="d-flex flex-column align-items-center border-end me-2" style="width: 20%;">
                                <span class="fw-light fs-6">Page</span>
                                <span class="fw-semibold fs-4">36</span>
                            </div>
                            <div class="fw-semibold" style="width: 80%;">
                                Kemukakan Pendapat di Dialog Mahasiswa
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="800">
                    <img src="{{ asset('img/majalah3.png') }}" alt="" class="w-100 mb-3 h-250">
                    <div class="mb-3">
                        <div class="w-100 d-flex">
                            <div class="d-flex flex-column align-items-center border-end me-2" style="width: 20%;">
                                <span class="fw-light fs-6">Page</span>
                                <span class="fw-semibold fs-4">37</span>
                            </div>
                            <div class="fw-semibold" style="width: 80%;">
                                Era Belajar Bersama Kampus Merdeka
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="w-100 py-5">
        <div class="container">
            <div class="w-100 mb-5">
                <h5 class="fw-bold mb-3" style="color: #0A4C7F;">STATISTICS ALUMNI</h5>
                <div class="rounded" style="background-color: #E9974B;width:80px;height:6px;"></div>
            </div>
            <figure class="highcharts-figure w-100">
                <div id="container" class="w-100"></div>
            </figure>
        </div>
    </section>
@endsection
@section('css')
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />

    <style>
        .h-108 {
            height: 108px;
        }

        .h-250 {
            height: 250px;
        }

        .top-50-persen {
            top: 50%;
            transform: translateY(-50%);
        }

        .bg-ukuran-center {
            background-position: center;
            background-size: cover;
        }

        .bg-ukuran {
            background-position: inherit;
            background-size: cover;
        }

        .link-top {
            text-decoration: none;
        }

        .link-top:hover {
            text-decoration: underline;
        }

        .btn-primary-1 {
            border: none;
            outline: none;
            color: white;
            background-color: #073455;
        }

        .swiper {
            width: 100%;
            height: 100%;
        }
    </style>
    <style>
        .highcharts-figure,
        .highcharts-data-table table {
            margin: 1em auto;
        }

        .highcharts-data-table table {
            font-family: Verdana, sans-serif;
            border-collapse: collapse;
            border: 1px solid #ebebeb;
            margin: 10px auto;
            text-align: center;
            width: 100%;
            max-width: 500px;
        }

        .highcharts-data-table caption {
            padding: 1em 0;
            font-size: 1.2em;
            color: #555;
        }

        .highcharts-data-table th {
            font-weight: 600;
            padding: 0.5em;
        }

        .highcharts-data-table td,
        .highcharts-data-table th,
        .highcharts-data-table caption {
            padding: 0.5em;
        }

        .highcharts-data-table thead tr,
        .highcharts-data-table tr:nth-child(even) {
            background: #f8f8f8;
        }

        .highcharts-data-table tr:hover {
            background: #f1f7ff;
        }
    </style>
@endsection
@section('js')
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/drilldown.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>

    <script>
        AOS.init();

        var swiper = new Swiper(".mySwiper", {
            autoplay: {
                delay: 2500,
                disableOnInteraction: false,
            },
            loop: true,
            slidesPerView: 1,
            spaceBetween: 10,
        });

        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Statistics Alumni'
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -30,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Total Alumni'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: 'Jumlah Alumni : <b>{point.y:.1f} Alumni</b>'
            },
            series: [{
                name: 'Alumni USM',
                data: [{
                        name: "Fakultas Hukum",
                        y: 3945,
                        color: 'rgb(255, 0, 0)'
                    }, {
                        name: "Fakultas Ekonomi",
                        y: 21839,
                        color: 'rgb(255, 255, 0)'
                    }, {
                        name: "Fakultas Teknik",
                        y: 7651,
                        color: 'rgb(22, 54, 92)'
                    },
                    {
                        name: "Fakultas Pertanian",
                        y: 2194,
                        color: 'rgb(0, 176, 80)'
                    },
                    {
                        name: "Fakultas Psikologi",
                        y: 2124,
                        color: 'rgb(112, 48, 160)'
                    },
                    {
                        name: "Fakultas TIK",
                        y: 4994,
                        color: 'rgb(255, 192, 0)'
                    },
                    {
                        name: "Magister Hukum",
                        y: 343,
                        color: 'rgb(255, 0, 0)'
                    },
                    {
                        name: "Magister Manajemen",
                        y: 1658,
                        color: 'rgb(255, 255, 0)'
                    },
                    {
                        name: "Magister Psikologi",
                        y: 39,
                        color: 'rgb(112, 48, 160)'
                    },
                ],
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    format: '{point.y:.1f}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });
    </script>
@endsection
