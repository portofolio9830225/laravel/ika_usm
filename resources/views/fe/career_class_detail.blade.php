@extends('fe.components.main')
@section('content')
    @include('fe.components.breadcrumbs', [
        'title' => $title,
        'list' => 3,
        'list_nama' => 'Lowongan Kerja',
    ])

    <div class="w-100 py-4">
        <div class="container">
            <div class="row">
                <div class="col-md-8 mb-3">
                    <h5 class="fw-bold mb-3 text-uppercase" style="color: #0A4C7F;">Virtual Campus Hiring PT Porto Indonesia
                        Sejahtera dengan Universitas Semarang</h5>
                    <p class=" fw-light">Career Class & Sharing&nbsp;&nbsp;|&nbsp;&nbsp;<i class="far fa-clock"></i>&nbsp;23
                        September 2022</p>
                    <img src="https://images.unsplash.com/photo-1667372633033-98e4be7ccb8b?crop=entropy&cs=tinysrgb&fit=crop&fm=jpg&h=300&ixid=MnwxfDB8MXxyYW5kb218MHx8fHx8fHx8MTY2ODg2MTYzNQ&ixlib=rb-4.0.3&q=80&w=800"
                        alt="" class="img-fluid mb-3">
                    <div class="mb-3 fw-light">
                        <p class="">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ipsum, tempora est
                            eveniet laboriosam
                            rem aperiam doloribus temporibus repudiandae magnam quaerat nisi non iusto? Quos blanditiis quo
                            porro voluptatem, placeat dignissimos?</p>
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Odio maxime temporibus sapiente quaerat
                            dignissimos pariatur unde! Officiis minima voluptatum a nisi. Exercitationem atque earum saepe
                            nostrum. Architecto vero accusantium, natus nesciunt, sint maiores veritatis numquam tenetur
                            distinctio soluta suscipit illum.</p>
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Odio maxime temporibus sapiente quaerat
                            dignissimos pariatur unde! Officiis minima voluptatum a nisi. Exercitationem atque earum saepe
                            nostrum. Architecto vero accusantium, natus nesciunt, sint maiores veritatis numquam tenetur
                            distinctio soluta suscipit illum.</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="mb-5">
                        <h5 class="fw-bold mb-3 text-uppercase" style="color: #0A4C7F;">Postingan Loker</h5>
                        <div class="rounded" style="background-color: #E9974B;width:80px;height:6px;"></div>
                        <div class="mt-3">
                            <div class="row mb-3 border-bottom pb-3">
                                <div class="col-4">
                                    <img src="{{ asset('img/loker/pameo.png') }}" alt="" class="w-100">
                                </div>
                                <div class="col-8">
                                    <p class="fw-semibold fs-6">PT Nihon Seiki Indonesia
                                    </p>
                                    <span class="fw-light"><i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;23 September
                                        2022</span>
                                </div>
                            </div>
                            <div class="row mb-3 border-bottom pb-3">
                                <div class="col-4">
                                    <img src="{{ asset('img/loker/pameo.png') }}" alt="" class="w-100">
                                </div>
                                <div class="col-8">
                                    <p class="fw-semibold fs-6">PT Nihon Seiki Indonesia
                                    </p>
                                    <span class="fw-light"><i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;23 September
                                        2022</span>
                                </div>
                            </div>
                            <div class="row mb-3 border-bottom pb-3">
                                <div class="col-4">
                                    <img src="{{ asset('img/loker/pameo.png') }}" alt="" class="w-100">
                                </div>
                                <div class="col-8">
                                    <p class="fw-semibold fs-6">PT Nihon Seiki Indonesia
                                    </p>
                                    <span class="fw-light"><i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;23 September
                                        2022</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mb-4">
                        <h5 class="fw-bold mb-3 text-uppercase" style="color: #0A4C7F;">Postingan Berita</h5>
                        <div class="rounded" style="background-color: #E9974B;width:80px;height:6px;"></div>
                        <div class="mt-3">
                            <div class="row mb-3 border-bottom pb-3">
                                <div class="col-4">
                                    <div class="w-100 h-100 bg-center"
                                        style="background-image:url({{ asset('img/content/usm1.jpg') }})"></div>
                                </div>
                                <div class="col-8">
                                    <p class="fw-semibold fs-6">IKA USM Memiliki Kepengurusan Baru dari Munas
                                    </p>
                                    <span class="fw-light"><i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;23 September
                                        2022</span>
                                </div>
                            </div>
                            <div class="row mb-3 border-bottom pb-3">
                                <div class="col-4">
                                    <div class="w-100 h-100 bg-center"
                                        style="background-image:url({{ asset('img/content/berita-22.png') }})"></div>
                                </div>
                                <div class="col-8">
                                    <p class="fw-semibold fs-6">Virtual Campus Hiring PT Porto Indonesia Sejahtera dengan
                                        Universitas Semarang
                                    </p>
                                    <span class="fw-light"><i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;23 September
                                        2022</span>
                                </div>
                            </div>
                            <div class="row mb-3 border-bottom pb-3">
                                <div class="col-4">
                                    <div class="w-100 h-100 bg-center"
                                        style="background-image:url({{ asset('img/content/usm1.jpg') }})"></div>
                                </div>
                                <div class="col-8">
                                    <p class="fw-semibold fs-6">Universitas Semarang mengadakan Seminar Internasional Tracer
                                        Study
                                    </p>
                                    <span class="fw-light"><i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;23 September
                                        2022</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
