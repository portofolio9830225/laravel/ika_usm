@extends('fe.components.main')
@section('content')
    @include('fe.components.breadcrumbs', ['title' => $title])

    <div class="w-100 py-4">
        <div class="container">
            <div class="row">
                <div class="col-md-8 mb-3">
                    <div class="mb-5">
                        <h5 class="fw-bold mb-3 text-uppercase" style="color: #0A4C7F;">{{ $title }}</h5>
                        <div class="rounded" style="background-color: #E9974B;width:80px;height:6px;"></div>
                    </div>

                    <div class="">
                        <a href="" class="text-decoration-none" style="color: black;">
                            <div class="row mb-4 border-bottom pb-2">
                                <div class="col-4">
                                    <img src="{{ asset('img/content/berita-22.png') }}" alt="" class="w-100">
                                </div>
                                <div class="col-8">
                                    <p class="fw-semibold fs-6 mb-2">Virtual Campus Hiring PT Porto Indonesia Sejahtera
                                        dengan
                                        Universitas Semarang
                                    </p>
                                    <p class="mb-2 fw-normal">
                                        Career Class & Sharing | <i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;23 September
                                        2022
                                    </p>
                                    <p class="fw-light mb-0 text-80">Lorem ipsum dolor sit amet consectetur. Feugiat hac
                                        vitae
                                        pharetra
                                        nisi. Neque
                                        lacinia quis non mattis tellus at eget aliquet. Mauris nulla egestas non
                                        pellentesque nunc elementum a. Ante a mauris risus sed egestas leo. Eget facilisi
                                        maecenas mollis nisi suscipit hac. Dui tortor donec.</p>
                                </div>
                            </div>
                        </a>
                        <a href="" class="text-decoration-none" style="color: black;">
                            <div class="row mb-4 border-bottom pb-2">
                                <div class="col-4">
                                    <img src="{{ asset('img/content/usm1.jpg') }}" alt="" class="w-100">
                                </div>
                                <div class="col-8">
                                    <p class="fw-semibold fs-6 mb-2">Virtual Campus Hiring PT Porto Indonesia Sejahtera
                                        dengan
                                        Universitas Semarang
                                    </p>
                                    <p class="mb-2 fw-normal">
                                        Career Class & Sharing | <i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;23 September
                                        2022
                                    </p>
                                    <p class="fw-light mb-0 text-80">Lorem ipsum dolor sit amet consectetur. Feugiat hac
                                        vitae
                                        pharetra
                                        nisi. Neque
                                        lacinia quis non mattis tellus at eget aliquet. Mauris nulla egestas non
                                        pellentesque nunc elementum a.</p>
                                </div>
                            </div>
                        </a>
                    </div>

                </div>
                <div class="col-md-4">
                    <div class="mb-5">
                        <h5 class="fw-bold mb-3 text-uppercase" style="color: #0A4C7F;">Postingan Loker</h5>
                        <div class="rounded" style="background-color: #E9974B;width:80px;height:6px;"></div>
                        <div class="mt-3">
                            <div class="row mb-3 border-bottom pb-3">
                                <div class="col-4">
                                    <img src="{{ asset('img/loker/pameo.png') }}" alt="" class="w-100">
                                </div>
                                <div class="col-8">
                                    <p class="fw-semibold fs-6">PT Nihon Seiki Indonesia
                                    </p>
                                    <span class="fw-light"><i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;23 September
                                        2022</span>
                                </div>
                            </div>
                            <div class="row mb-3 border-bottom pb-3">
                                <div class="col-4">
                                    <img src="{{ asset('img/loker/pameo.png') }}" alt="" class="w-100">
                                </div>
                                <div class="col-8">
                                    <p class="fw-semibold fs-6">PT Nihon Seiki Indonesia
                                    </p>
                                    <span class="fw-light"><i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;23 September
                                        2022</span>
                                </div>
                            </div>
                            <div class="row mb-3 border-bottom pb-3">
                                <div class="col-4">
                                    <img src="{{ asset('img/loker/pameo.png') }}" alt="" class="w-100">
                                </div>
                                <div class="col-8">
                                    <p class="fw-semibold fs-6">PT Nihon Seiki Indonesia
                                    </p>
                                    <span class="fw-light"><i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;23 September
                                        2022</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mb-4">
                        <h5 class="fw-bold mb-3 text-uppercase" style="color: #0A4C7F;">Postingan Berita</h5>
                        <div class="rounded" style="background-color: #E9974B;width:80px;height:6px;"></div>
                        <div class="mt-3">
                            <div class="row mb-3 border-bottom pb-3">
                                <div class="col-4">
                                    <div class="w-100 h-100 bg-center"
                                        style="background-image:url({{ asset('img/content/usm1.jpg') }})"></div>
                                </div>
                                <div class="col-8">
                                    <p class="fw-semibold fs-6">IKA USM Memiliki Kepengurusan Baru dari Munas
                                    </p>
                                    <span class="fw-light"><i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;23 September
                                        2022</span>
                                </div>
                            </div>
                            <div class="row mb-3 border-bottom pb-3">
                                <div class="col-4">
                                    <div class="w-100 h-100 bg-center"
                                        style="background-image:url({{ asset('img/content/berita-22.png') }})"></div>
                                </div>
                                <div class="col-8">
                                    <p class="fw-semibold fs-6">Virtual Campus Hiring PT Porto Indonesia Sejahtera dengan
                                        Universitas Semarang
                                    </p>
                                    <span class="fw-light"><i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;23 September
                                        2022</span>
                                </div>
                            </div>
                            <div class="row mb-3 border-bottom pb-3">
                                <div class="col-4">
                                    <div class="w-100 h-100 bg-center"
                                        style="background-image:url({{ asset('img/content/usm1.jpg') }})"></div>
                                </div>
                                <div class="col-8">
                                    <p class="fw-semibold fs-6">Universitas Semarang mengadakan Seminar Internasional Tracer
                                        Study
                                    </p>
                                    <span class="fw-light"><i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;23 September
                                        2022</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <style>
        .bg-center {
            background-position: center;
            background-size: cover;
        }

        .text-80 {
            font-size: 90%;
        }
    </style>
@endsection
