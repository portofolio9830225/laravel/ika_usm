@extends('fe.components.main')
@section('content')
    @include('fe.components.breadcrumbs', ['title' => $title])

    <div class="w-100 py-4">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="mb-5">
                        <h5 class="fw-bold mb-3 text-uppercase" style="color: #0A4C7F;">{{ $title }}</h5>
                        <div class="rounded" style="background-color: #E9974B;width:80px;height:6px;"></div>
                    </div>

                    <div class="w-100">
                        <div class="w-100 d-flex justify-content-center border-bottom mb-3">
                            <div class="">
                                <h5 class="fw-bold mb-3 text-uppercase d-block" style="color: #0A4C7F;">2022</h5>
                                <div class="rounded pt-1" style="background-color: #E9974B;">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 mb-3">
                                <img src="{{ asset('img/alumni/alumni1.png') }}" alt="" class="img-fluid mb-2">
                                <h6 class="fw-bold">Dyah Ayu Anggun Kusumawardani</h6>
                                <p class="mb-0 fw-light" style="font-size: 80%;">Head of market strategy in Tokopedia</p>
                            </div>
                            <div class="col-md-3 mb-3">
                                <img src="{{ asset('img/alumni/alumni2.png') }}" alt="" class="img-fluid mb-2">
                                <h6 class="fw-bold">Firsto Joan</h6>
                                <p class="mb-0 fw-light" style="font-size: 80%;">Full stack Developer at Bukalapak</p>
                            </div>
                            <div class="col-md-3 mb-3">
                                <img src="{{ asset('img/alumni/alumni3.png') }}" alt="" class="img-fluid mb-2">
                                <h6 class="fw-bold">Wilian Muhammad Farhan</h6>
                                <p class="mb-0 fw-light" style="font-size: 80%;">Progammer at Bukalapak</p>
                            </div>
                            <div class="col-md-3 mb-3">
                                <img src="{{ asset('img/alumni/alumni4.png') }}" alt="" class="img-fluid mb-2">
                                <h6 class="fw-bold">Fadlillah Dzikir Imania</h6>
                                <p class="mb-0 fw-light" style="font-size: 80%;">Marketing at CV. ABC</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <style>
        .bg-center {
            background-position: center;
            background-size: cover;
        }
    </style>
@endsection
