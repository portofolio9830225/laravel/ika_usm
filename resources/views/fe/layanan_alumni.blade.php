@extends('fe.components.main')
@section('content')
    @include('fe.components.breadcrumbs', ['title' => $title])

    <div class="w-100 py-4">
        <div class="container">
            <div class="row">
                <div class="col-md-8 mb-3">
                    <div class="mb-4">
                        <h5 class="fw-bold mb-3 text-uppercase" style="color: #0A4C7F;">{{ $title }}</h5>
                        <div class="rounded" style="background-color: #E9974B;width:80px;height:6px;"></div>
                    </div>
                    <img src="{{ asset('img/alumni1.png') }}" alt="" class="img-fluid mb-3">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel laborum officia quas voluptate
                        aspernatur quibusdam perferendis earum, ipsa asperiores alias quo ipsam nulla est non quasi ea
                        cumque voluptates ab?</p>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem, rerum maiores iste impedit
                        eveniet nostrum ducimus perferendis non numquam, enim, laboriosam consectetur dolore id. Officia
                        quisquam placeat magni incidunt sit aperiam natus vitae aut quos iusto veniam commodi, fugit nulla.
                    </p>
                </div>
                <div class="col-md-4">
                    <div class="mb-5">
                        <h5 class="fw-bold mb-3 text-uppercase" style="color: #0A4C7F;">Postingan Loker</h5>
                        <div class="rounded" style="background-color: #E9974B;width:80px;height:6px;"></div>
                        <div class="mt-3">
                            <div class="row mb-3 border-bottom pb-3">
                                <div class="col-4">
                                    <img src="{{ asset('img/loker/pameo.png') }}" alt="" class="w-100">
                                </div>
                                <div class="col-8">
                                    <p class="fw-semibold fs-6">PT Nihon Seiki Indonesia
                                    </p>
                                    <span class="fw-light"><i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;23 September
                                        2022</span>
                                </div>
                            </div>
                            <div class="row mb-3 border-bottom pb-3">
                                <div class="col-4">
                                    <img src="{{ asset('img/loker/pameo.png') }}" alt="" class="w-100">
                                </div>
                                <div class="col-8">
                                    <p class="fw-semibold fs-6">PT Nihon Seiki Indonesia
                                    </p>
                                    <span class="fw-light"><i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;23 September
                                        2022</span>
                                </div>
                            </div>
                            <div class="row mb-3 border-bottom pb-3">
                                <div class="col-4">
                                    <img src="{{ asset('img/loker/pameo.png') }}" alt="" class="w-100">
                                </div>
                                <div class="col-8">
                                    <p class="fw-semibold fs-6">PT Nihon Seiki Indonesia
                                    </p>
                                    <span class="fw-light"><i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;23 September
                                        2022</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mb-4">
                        <h5 class="fw-bold mb-3 text-uppercase" style="color: #0A4C7F;">Postingan Berita</h5>
                        <div class="rounded" style="background-color: #E9974B;width:80px;height:6px;"></div>
                        <div class="mt-3">
                            <div class="row mb-3 border-bottom pb-3">
                                <div class="col-4">
                                    <div class="w-100 h-100 bg-center"
                                        style="background-image:url({{ asset('img/content/usm1.jpg') }})"></div>
                                </div>
                                <div class="col-8">
                                    <p class="fw-semibold fs-6">IKA USM Memiliki Kepengurusan Baru dari Munas
                                    </p>
                                    <span class="fw-light"><i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;23 September
                                        2022</span>
                                </div>
                            </div>
                            <div class="row mb-3 border-bottom pb-3">
                                <div class="col-4">
                                    <div class="w-100 h-100 bg-center"
                                        style="background-image:url({{ asset('img/content/berita-22.png') }})"></div>
                                </div>
                                <div class="col-8">
                                    <p class="fw-semibold fs-6">Virtual Campus Hiring PT Porto Indonesia Sejahtera dengan
                                        Universitas Semarang
                                    </p>
                                    <span class="fw-light"><i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;23 September
                                        2022</span>
                                </div>
                            </div>
                            <div class="row mb-3 border-bottom pb-3">
                                <div class="col-4">
                                    <div class="w-100 h-100 bg-center"
                                        style="background-image:url({{ asset('img/content/usm1.jpg') }})"></div>
                                </div>
                                <div class="col-8">
                                    <p class="fw-semibold fs-6">Universitas Semarang mengadakan Seminar Internasional Tracer
                                        Study
                                    </p>
                                    <span class="fw-light"><i class="far fa-clock"></i>&nbsp;&nbsp;&nbsp;23 September
                                        2022</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
