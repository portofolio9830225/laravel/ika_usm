<li class="nav-item">
    <a href="{{ route('home') }}">
        <p>Home</p>
    </a>
</li>
<li class="nav-item">
    <a data-toggle="collapse" href="#Master" class="collapsed" aria-expanded="false">
        <p>Master</p>
        <span class="caret"></span>
    </a>
    <div class="collapse" id="Master">
        <ul class="nav nav-collapse">
            <li>
                <a href="{{ route('slide.index') }}">
                    <span class="sub-item">Slider</span>
                </a>
            </li>
            <li>
                <a href="{{ route('alumni.index') }}">
                    <span class="sub-item">Alumni</span>
                </a>
            </li>
            <li>
                <a href="{{ route('news.index') }}">
                    <span class="sub-item">News Alumni</span>
                </a>
            </li>
            <li>
                <a href="{{ route('tips-karier.index') }}">
                    <span class="sub-item">Tips Karier</span>
                </a>
            </li>
        </ul>
    </div>
</li>
<li class="nav-item">
    <a data-toggle="collapse" href="#Loker" class="collapsed" aria-expanded="false">
        <p>Lowongan</p>
        <span class="caret"></span>
    </a>
    <div class="collapse" id="Loker">
        <ul class="nav nav-collapse">
            <li>
                <a href="{{ route('kerja.index') }}">
                    <span class="sub-item">Kerja</span>
                </a>
            </li>
            <li>
                <a href="http://bpm_sistem.test/fakultas">
                    <span class="sub-item">Magang</span>
                </a>
            </li>
        </ul>
    </div>
</li>
<li class="nav-item">
    <a data-toggle="collapse" href="#Profile" class="collapsed" aria-expanded="false">
        <p>Profile</p>
        <span class="caret"></span>
    </a>
    <div class="collapse" id="Profile">
        <ul class="nav nav-collapse">
            <li>
                <a href="{{ route('kerja.index') }}">
                    <span class="sub-item">Profile</span>
                </a>
            </li>
            <li>
                <a href="{{ route('visi-misi.index') }}">
                    <span class="sub-item">Visi dan Misi</span>
                </a>
            </li>
        </ul>
    </div>
</li>
