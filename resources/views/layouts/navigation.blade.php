@php
// $menu = session('menu');
$menu = menu();
@endphp
<ul class="nav nav-primary">
    @foreach ($menu as $item)
        @if (isset($item->sub_menu))
            <li class="nav-item">
                <a data-toggle="collapse" href="#{{ $item->name_menu }}" class="collapsed" aria-expanded="false">
                    <p>{{ $item->name_menu }}</p>
                    <span class="caret"></span>
                </a>
                <div class="collapse" id="{{ $item->name_menu }}">
                    <ul class="nav nav-collapse">
                        @foreach ($item->sub_menu as $val)
                            <li>
                                <a href="{{ url($val->link) }}">
                                    <span class="sub-item">{{ $val->name_menu }}</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </li>
        @else
            <li class="nav-item">
                <a href="{{ url($item->link) }}">
                    <p>{{ $item->name_menu }}</p>
                </a>
            </li>
        @endif
    @endforeach
</ul>
