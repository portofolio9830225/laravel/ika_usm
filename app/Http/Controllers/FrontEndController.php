<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontEndController extends Controller
{
    public function index()
    {
        return view('fe.index');
    }

    public function kegiatan_ika()
    {
        $data = [
            'title' => 'Kegiatan IKA'
        ];

        return view('fe.kegiatan_ika', $data);
    }

    public function tips_karier()
    {
        $data = [
            'title' => 'Tips Karier'
        ];

        return view('fe.tips_karier', $data);
    }

    public function career_class_detail($slug)
    {
        $data = [
            'title' => 'Career Class & Sharing'
        ];

        return view('fe.career_class_detail', $data);
    }

    public function struktur_organisasi()
    {
        $data = [
            'title' => 'Struktur Organisasi'
        ];

        return view('fe.struktur_organisasi', $data);
    }

    public function profil_directori_staff()
    {
        $data = [
            'title' => 'Profile'
        ];

        return view('fe.profil_directori_staff', $data);
    }

    public function kartu_tanda_alumni()
    {
        $data = [
            'title' => 'Kartu Tanda Alumni'
        ];

        return view('fe.kartu_tanda_alumni', $data);
    }

    public function lowongan_kerja()
    {
        $data = [
            'title' => 'Lowongan Kerja'
        ];

        return view('fe.lowongan_kerja', $data);
    }

    public function lowongan_kerja_detail($slug)
    {
        $data = [
            'title' => 'PT Infomedia Nusantara'
        ];

        return view('fe.lowongan_kerja_detail', $data);
    }

    public function lowongan_magang()
    {
        $data = [
            'title' => 'Lowongan Magang'
        ];

        return view('fe.lowongan_magang', $data);
    }

    public function alumni_unggul()
    {
        $data = [
            'title' => 'Alumni Unggul'
        ];

        return view('fe.alumni_unggul', $data);
    }

    public function news_alumni()
    {
        $data = [
            'title' => 'News Alumni'
        ];

        return view('fe.news_alumni', $data);
    }

    public function contact_us()
    {
        $data = [
            'title' => 'Kontak Kami'
        ];

        return view('fe.contact_us', $data);
    }

    public function layanan_alumni()
    {
        $data = [
            'title' => 'Layanan Alumni'
        ];

        return view('fe.layanan_alumni', $data);
    }

    public function layanan_karier()
    {
        $data = [
            'title' => 'Layanan Karier'
        ];

        return view('fe.layanan_karier', $data);
    }
}
