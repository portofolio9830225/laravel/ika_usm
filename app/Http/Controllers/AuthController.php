<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function index()
    {
        return view('auth.login');
    }

    public function auth()
    {
        // return request();
        $user = User::firstWhere('email', request()->email);

        if (!$user) {
            return back()->with('error', 'Username / Password Salah');
        }

        $auth = [
            'email'     => $user->email,
            'password'  => $user->salt . request()->password
        ];

        if (Auth::attempt($auth)) {
            request()->session()->regenerate();

            return redirect()->route('home');
        }

        return back()->with('error', 'Username / Password Salah');
    }

    public function logout()
    {
        Auth::logout();

        request()->session()->invalidate();

        request()->session()->regenerateToken();

        return redirect('/');
    }
}
