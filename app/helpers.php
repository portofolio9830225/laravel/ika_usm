<?php

use Illuminate\Support\Facades\Crypt;

function enc($val)
{
    return Crypt::encrypt($val);
}

function dec($val)
{
    return Crypt::decrypt($val);
}

function initialAction()
{
    successAction();
    deleteAction();
}

function successAction()
{
    if (session()->has('success')) {
?>
        <script>
            swal("Success", "<?= session('success') ?>", {
                icon: "success",
                buttons: {
                    confirm: {
                        className: 'btn btn-success'
                    }
                },
            });
        </script>

    <?php
    }
}

function infoAction()
{
    if (session()->has('info')) {
    ?>
        <script>
            swal("Info", "<?= session('info') ?>", {
                icon: "info",
                buttons: {
                    confirm: {
                        className: 'btn btn-info'
                    }
                },
            });
        </script>

    <?php
    }
}

function deleteAction()
{
    ?>
    <form method="POST" id="destroy" style="display: none">
        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
        <input type="hidden" name="_method" value="DELETE">
    </form>

    <script>
        function deleteData(url) {
            swal({
                title: 'Anda Yakin Ingin Hapus Data Ini?',
                text: "Data Ini Akan Terhapus Selamanya Dari Sistem!",
                type: 'warning',
                buttons: {
                    confirm: {
                        text: 'Hapus!',
                        className: 'btn btn-success'
                    },
                    cancel: {
                        visible: true,
                        text: 'Batal',
                        className: 'btn btn-danger'
                    }
                }
            }).then((Delete) => {
                if (Delete) {
                    $("#destroy").attr('action', url);
                    $("#destroy").submit();
                }
            });
        }
    </script>

<?php
}

function urlLoker($file = "")
{
    if ($file == "") {
        return asset('img/no_image.jpg');
    } else {
        return asset('img/loker/' . $file);
    }
}

function urlPost($file = "")
{
    if ($file == "") {
        return asset('img/no_image.jpg');
    } else {
        return asset('img/content/' . $file);
    }
}
