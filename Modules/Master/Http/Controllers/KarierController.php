<?php

namespace Modules\Master\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Image;
use Modules\Master\Entities\Post;

class KarierController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $data = [
            'title'     => 'Tips Karier',
        ];

        return view('master::karier.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $data = [
            'title'     => 'Tips Karier',
        ];

        return view('master::karier.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        DB::beginTransaction();

        if (request()->hasFile('file')) {
            $image = $request->file('file');
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(410, 270);
            $nama = time() . rand(1, 100) . '.' . $image->getClientOriginalExtension();
            $image_resize->save(public_path('img/content/' . $nama));
        }

        $data = Arr::except(request()->all(), ['_token', 'file']);
        $data['jenis_post'] = 'karier';
        $data['file']       = $nama;
        Post::create($data);

        DB::commit();

        return redirect()->route('news.index')->with('success', 'Berita berhasil Di-buat');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('master::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('master::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
