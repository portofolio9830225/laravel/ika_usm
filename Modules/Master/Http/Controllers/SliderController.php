<?php

namespace Modules\Master\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Master\Entities\Slide;
use File;
use Illuminate\Support\Facades\DB;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $data = [
            'title'         => 'Slide',
            'slide_active'  => Slide::where('is_active', 1)->latest()->get(),
            'slide'         => Slide::latest()->get()
        ];

        // return $data['slide_active'];

        return view('master::slide.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('master::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        // return request();

        $foto = request()->file('file');
        $nama = time() . rand(1, 100) . '.' . $foto->getClientOriginalExtension();
        $foto->move('img/slider/', $nama);

        Slide::create([
            'file'      => $nama,
            'is_active' => isset(request()->active) ? true : false
        ]);

        return back();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('master::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('master::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        $slide =  Slide::find(dec($id));
        File::delete('img/slider/' . $slide->file);

        $slide->delete();
        DB::commit();
        return back()->with('success', 'Slide berhasil Di-hapus');
    }

    public function update_all()
    {
        // return request();
        $slide = Slide::get();

        if (isset(request()->slide)) {
            foreach ($slide as $item) {
                if (in_array($item->id, request()->slide)) {
                    Slide::find($item->id)->update(['is_active' => 1]);
                } else {
                    Slide::find($item->id)->update(['is_active' => 0]);
                }
            }

            return back()->with('success', 'Slide berhasil Di-update');
        } else {

            foreach ($slide as $item) {
                Slide::find($item->id)->update(['is_active' => 0]);
            }

            return back()->with('success', 'Slide berhasil Di-update');
        }
    }
}
