<?php

namespace Modules\Master\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Modules\Master\Entities\Alumni;

class AlumniController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $data = [
            'title'     => 'Alumni',
            'alumni'    => Alumni::latest()->get()
        ];

        return view('master::alumni.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('master::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        // return request();
        $data = Arr::except(request()->all(), ['file', '_token']);

        if (request()->hasFile('file')) {
            $foto = request()->file('file');
            $nama = time() . rand(1, 100) . '.' . $foto->getClientOriginalExtension();
            $foto->move('file/', $nama);

            $data['file'] = $nama;
        }

        // return $data;
        Alumni::create($data);
        return back()->with('success', 'List Alumni berhasil Di-tambahkan');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('master::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('master::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        // return dec($id);
        DB::beginTransaction();
        Alumni::destroy(dec($id));
        DB::commit();

        return back()->with('success', 'List Alumni berhasil Di-hapus');
    }

    public function download($id)
    {
        $alumni = Alumni::findOrFail(dec($id));
        return response()->download(public_path('file/' . $alumni->file));
    }
}
