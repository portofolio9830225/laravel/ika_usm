@extends('layouts.main')
@section('breadcrumb')
    @include('layouts.breadcrumb')
@endsection
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="w-100 d-flex justify-content-between mb-3">
                <h3>{{ $title }}</h3>
                <div class="">
                    <a href="{{ route('layanan-alumni.preview') }}" class="btn btn-outline-info btn-sm"><i
                            class="fa fa-eye"></i>&nbsp;Preview</a>
                </div>
            </div>
            <hr />
            <form action="{{ route('layanan-alumni.store') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" placeholder="Title" name="title"
                        value="">
                </div>
                <div class="form-group">
                    <label for="desc">Deskipsi</label>
                    <textarea name="desc" id="editor" class="form-control"></textarea>
                </div>
                <button class="btn btn-primary w-100">Save</button>
            </form>
        </div>
    </div>
@endsection
@section('js')
    @php
        initialAction();
    @endphp
    <script src="https://cdn.ckeditor.com/ckeditor5/35.3.1/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#editor'))
            .catch(error => {
                console.error(error);
            });
    </script>
@endsection
