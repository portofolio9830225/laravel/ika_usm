@extends('layouts.main')
@section('breadcrumb')
    @include('layouts.breadcrumb')
@endsection
@section('content')
    <div class="card">
        <div class="card-body">
            <h3>{{ $title }} Preview</h3>
            <div class="">
                <h5>Desc</h5>
                <p>{!! $layanan->desc !!}</p>
            </div>
        </div>
    </div>
@endsection
