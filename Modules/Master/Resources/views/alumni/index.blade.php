@extends('layouts.main')
@section('breadcrumb')
    @include('layouts.breadcrumb')
@endsection
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="w-100 d-flex justify-content-between mb-3">
                <h3>{{ $title }}</h3>
                <div class="">
                    <button type="button" class="btn btn-outline-primary btn-sm" data-toggle="modal"
                        data-target="#exampleModal">Tambah</button>
                </div>
            </div>
            <div class="table-responsive">
                <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th width="20px">No</th>
                            <th>Nama Fakultas</th>
                            <th>Color</th>
                            <th>Total</th>
                            <th>File</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($alumni as $item)
                            <tr>
                                <th>{{ $loop->iteration }}</th>
                                <td>{{ $item->nama_fakultas }}</td>
                                <td>
                                    <div class="w-100" style="background-color: {{ $item->color }};height:20px;"></div>
                                </td>
                                <td>{{ $item->total }}</td>
                                <td>
                                    <button class="btn btn-sm btn-primary"
                                        onclick="window.location='{{ route('alumni.download', enc($item->id)) }}'">Download</button>
                                </td>
                                <td>
                                    <a class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top"
                                        title="Edit"><i class="fa fa-edit"></i></a>
                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="tooltip"
                                        data-placement="top" title="Delete"
                                        onclick="deleteData('{{ route('alumni.destroy', enc($item->id)) }}')"><i
                                            class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Alumni</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('alumni.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="nama_fakultas">Nama Fakultas</label>
                            <input type="text" class="form-control" id="nama_fakultas" placeholder="Nama Fakultas"
                                name="nama_fakultas" required>
                        </div>
                        <div class="form-group">
                            <label for="color" class="d-block">Color</label>
                            <input type="color" id="color" name="color" required>
                        </div>
                        <div class="form-group">
                            <label for="total_alumni">Total Alumni</label>
                            <input type="text" class="form-control" id="total_alumni" placeholder="0" name="total"
                                required>
                        </div>
                        <div class="form-group">
                            <label for="file">File</label>
                            <input type="file" class="form-control" id="file" name="file" required
                                accept=".pdf">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
{{-- @section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/dataTables.bootstrap4.min.css">
@endsection --}}
@section('js')
    @php
        initialAction();
    @endphp
    {{-- <script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.1/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        });
    </script> --}}
@endsection
