@extends('layouts.main')
@section('breadcrumb')
    @include('layouts.breadcrumb')
@endsection
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="w-100 d-flex justify-content-between mb-3">
                <h3>{{ $title }}</h3>
                <div class="">
                    <button type="button" class="btn btn-outline-primary btn-sm" data-toggle="modal"
                        data-target="#exampleModal">Tambah</button>
                </div>
            </div>
            @if (count($slide) > 0 && count($slide_active) > 0)
                <div class="swiper mySwiper">
                    <div class="swiper-wrapper">
                        @foreach ($slide_active as $item)
                            <div class="swiper-slide">
                                <img src="{{ asset('img/slider/' . $item->file) }}" alt="" class="img-fluid">
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
            <hr />
            @if (count($slide) > 0)
                <form action="{{ route('slide.update.all') }}" method="post">
                    @csrf
                    @method('put')
                    <div class="row mb-3">
                        @foreach ($slide as $item)
                            <div class="col-6 col-sm-4">
                                <label class="imagecheck">
                                    <input name="slide[]" type="checkbox" value="{{ $item->id }}"
                                        {{ $item->is_active ? 'checked' : '' }} class="imagecheck-input">
                                    <figure class="imagecheck-figure">
                                        <img src="{{ asset('img/slider/' . $item->file) }}" alt="title"
                                            class="imagecheck-image">
                                    </figure>
                                </label>
                                <button type="button" class="btn btn-danger w-100"
                                    onclick="deleteData('{{ route('slide.destroy', enc($item->id)) }}')"><i
                                        class="fa fa-trash"></i></button>
                            </div>
                        @endforeach
                    </div>
                    <button type="submit" class="btn btn-primary w-100">Save</button>
                </form>
            @else
                <div class="w-100 bg-light text-center py-2">Nothing Slide !!!</div>
            @endif
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Slide</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('slide.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="file">File</label>
                            <input type="file" class="form-control" id="file" accept="image/*"
                                onchange="loadFile(event)" name="file" required>
                            <small id="emailHelp" class="form-text text-muted">NB : Ukuran Size 1280x416 px</small>
                        </div>
                        <div class="form-group">
                            <input type="checkbox" name="active" id="active" checked>&nbsp;
                            <label for="active">Active</label>
                        </div>
                        <div class="" id="preview" style="display: none;">
                            <img src="" alt="" class="img-fluid" id="img-preview">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />
    <style>
        .swiper {
            width: 100%;
            height: 100%;
        }
    </style>
@endsection
@section('js')
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>

    @php
        initialAction();
    @endphp
    <script>
        var loadFile = function(event) {
            var output = document.getElementById('img-preview');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function() {
                URL.revokeObjectURL(output.src) // free memory
            }
            $('#preview').show();
        };

        new Swiper(".mySwiper", {
            autoplay: {
                delay: 2500,
                disableOnInteraction: false,
            },
            loop: true,
            slidesPerView: 1,
            spaceBetween: 10,
        });
    </script>
@endsection
