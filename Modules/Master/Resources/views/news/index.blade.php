@extends('layouts.main')
@section('breadcrumb')
    @include('layouts.breadcrumb')
@endsection
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="w-100 d-flex justify-content-between mb-3">
                <h3>{{ $title }}</h3>
                <div class="">
                    <button type="button" class="btn btn-outline-primary btn-sm"
                        onclick="window.location='{{ route('news.create') }}'">Tambah</button>
                </div>
            </div>
            <div class="table-responsive">
                <table id="basic-datatables" class="display table table-striped table-hover" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Judul</th>
                            <th>Photo</th>
                            <th>Date</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $item)
                            <tr>
                                <th>{{ $loop->iteration }}</th>
                                <td>{{ $item->judul }}</td>
                                <td><img src="{{ urlPost($item->file) }}" alt="" width="100px"
                                        class="img-thumbnail"></td>
                                <td>{{ $item->created_at->diffForHumans() }}</td>
                                <td>...</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('js')
    @php
        initialAction();
    @endphp
    <script>
        $('#basic-datatables').DataTable();
    </script>
@endsection
