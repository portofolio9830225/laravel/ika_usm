@extends('layouts.main')
@section('breadcrumb')
    @include('layouts.breadcrumb')
@endsection
@section('content')
    <div class="row">
        <div class="col-md-6">
            <form action="{{ route('news.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card">
                    <div class="card-body">
                        <div class="mb-3">
                            <label for="judul" class="form-label">Judul</label>
                            <input type="text" class="form-control" id="judul" name="judul" required>
                        </div>
                        <label for="judul" class="form-label mb-2">Photo</label>
                        <div class="mb-3">
                            <img src="{{ urlPost() }}" alt="" width="150px" id="img-preview">
                            <input type="file" class="form-control mt-2" id="file" name="file"
                                accept=".jpg,.png,.jpeg" onchange="loadFile(event)" required>
                            <small class="form-text text-muted">NB : Ukuran Size 410x270 px</small>
                        </div>
                        <div class="mb-3">
                            <label for="editor" class="form-label">Deskripsi</label>
                            <textarea name="deskripsi" id="editor" rows="3" class="form-control"></textarea>
                        </div>

                        <button type="submit" class="btn btn-primary">Kirim</button>
                        <button type="button" class="btn btn-default"
                            onclick="window.location='{{ url()->previous() }}'">Back</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('js')
    <script src="https://cdn.ckeditor.com/ckeditor5/35.3.1/classic/ckeditor.js"></script>
    <script>
        var loadFile = function(event) {
            var output = document.getElementById('img-preview');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function() {
                URL.revokeObjectURL(output.src) // free memory
            }
        }

        ClassicEditor
            .create(document.querySelector('#editor'))
            .catch(error => {
                console.error(error);
            });
    </script>
@endsection
