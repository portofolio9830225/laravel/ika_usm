<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Modules\Master\Http\Controllers\AlumniController;
use Modules\Master\Http\Controllers\BeritaController;
use Modules\Master\Http\Controllers\DashboardController;
use Modules\Master\Http\Controllers\KarierController;
use Modules\Master\Http\Controllers\LayananAlumniController;
use Modules\Master\Http\Controllers\SliderController;

Route::prefix('master')->group(function () {
    // Route::get('/', 'MasterController@index');

    Route::put('slide/all', [SliderController::class, 'update_all'])->name('slide.update.all');
    Route::resource('slide', SliderController::class);

    Route::get('alumni/download/{id}', [AlumniController::class, 'download'])->name('alumni.download');
    Route::resource('alumni', AlumniController::class);

    Route::get('layanan-alumni/preview', [LayananAlumniController::class, 'preview'])->name('layanan-alumni.preview');
    Route::resource('layanan-alumni', LayananAlumniController::class);

    Route::resource('news', BeritaController::class);

    Route::resource('tips-karier', KarierController::class);
});

Route::get('home', [DashboardController::class, 'index'])->name('home');
