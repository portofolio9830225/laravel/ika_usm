@extends('layouts.main')
@section('breadcrumb')
    @include('layouts.breadcrumb', ['title' => $title, 'sub_title' => 'Add'])
@endsection
@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{ route('kerja.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label for="judul" class="form-label">Nama Perusahaan</label>
                            <input type="text" class="form-control" name="nama_perusahaan" placeholder="Nama Perusahaan">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label for="judul" class="form-label">Alamat</label>
                            <input type="text" class="form-control" name="alamat" placeholder="Alamat">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="mb-3">
                            <label for="judul" class="form-label">Photo</label>
                            <input type="file" name="foto" class="form-control" accept=".jpg,.png,.jpeg"
                                onchange="loadFile(event)" required>
                            <div class="form-text">NB : Ukuran Size 410x270 px.</div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label for="" class="form-label">Photo Review</label>
                        <img src="{{ urlLoker() }}" alt="" class="img-fluid" id="img-preview">
                    </div>
                </div>
                <label class="form-label">Informasi Lowongan</label>
                <div class="row my-3" id="loker1">
                    <div class="col-md-5">
                        <input type="text" class="form-control" placeholder="key" name="key[]">
                    </div>
                    <div class="col-md-5">
                        <textarea name="val[]" id="editor" rows="3" class="form-control"></textarea>
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-success btn-sm" id="addLoker"><i
                                class="fa fa-plus"></i></button>
                    </div>
                </div>

                <button type="submit" class="btn btn-primary">Kirim</button>
                <button type="button" class="btn btn-default"
                    onclick="window.location='{{ url()->previous() }}'">Back</button>
            </form>
        </div>
    </div>
@endsection
@section('js')
    <script src="https://cdn.ckeditor.com/ckeditor5/35.3.1/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#editor'))
            .catch(error => {
                console.error(error);
            });
    </script>
    <script>
        var loadFile = function(event) {
            var output = document.getElementById('img-preview');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function() {
                URL.revokeObjectURL(output.src) // free memory
            }
        };

        let arr = [];

        function loadCkeditor(no) {
            ClassicEditor
                .create(document.querySelector(`#editor${no}`))
                .catch(error => {
                    console.error(error);
                });
        }

        $('body').on('click', '#addLoker', function() {
            const no = arr.length + 1;
            arr.push(no);

            const html = `<div class="row my-3" id="loker${no}">
                    <div class="col-md-5">
                        <input type="text" class="form-control" placeholder="key" name="key[]">
                    </div>
                    <div class="col-md-5">
                        <textarea name="val[]" id="editor${no}" rows="3" class="form-control"></textarea>
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-success btn-sm" id="addLoker"><i
                                class="fa fa-plus"></i></button>
                        <button type="button" class="btn btn-danger btn-sm" id="deleteLoker"><i
                                class="fa fa-trash"></i></button>
                    </div>
            </div>`;

            $(this).parent().parent().after(html);
            loadCkeditor(no);
        })

        $('body').on('click', '#deleteLoker', function() {
            $(this).parent().parent().remove();
        })
    </script>
@endsection
