<?php

namespace Modules\Lowongan\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Loker extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'loker';
}
