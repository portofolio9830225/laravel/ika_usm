<?php

namespace Modules\Lowongan\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class LokerValue extends Model
{
    use HasFactory;

    protected $table = 'loker_value';
    protected $guarded = [];
}
