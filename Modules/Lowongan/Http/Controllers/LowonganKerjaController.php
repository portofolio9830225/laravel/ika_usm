<?php

namespace Modules\Lowongan\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Image;
use Modules\Lowongan\Entities\Loker;
use Modules\Lowongan\Entities\LokerValue;

class LowonganKerjaController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $data = [
            'title' => 'Lowongan Kerja',
            'data'  => Loker::latest()->get()
        ];

        return view('lowongan::kerja.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $data = [
            'title' => 'Lowongan Kerja'
        ];
        return view('lowongan::kerja.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        // return request();

        DB::beginTransaction();
        $image = $request->file('foto');
        $image_resize = Image::make($image->getRealPath());
        $image_resize->resize(410, 270);
        $nama = time() . rand(1, 100) . '.' . $image->getClientOriginalExtension();
        $image_resize->save(public_path('img/loker/' . $nama));

        $data = Arr::only(request()->all(), ['nama_perusahaan', 'alamat']);
        $data['foto']   = $nama;
        $data['type']   = 1;
        $loker = Loker::firstOrCreate($data);

        if (isset(request()->key)) {
            for ($i = 0; $i < count(request()->key); $i++) {
                LokerValue::firstOrCreate([
                    'loker_id'  => $loker->id,
                    'key'       => request()->key[$i],
                    'value'     => request()->val[$i],
                ]);
            }
        }
        DB::commit();

        return redirect()->route('kerja.index')->with('success', 'Lowongan Kerja berhasil Di-buat');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('lowongan::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('lowongan::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
