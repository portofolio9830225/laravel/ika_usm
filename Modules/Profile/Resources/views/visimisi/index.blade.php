@extends('layouts.main')
@section('breadcrumb')
    @include('layouts.breadcrumb')
@endsection
@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('visi-misi.store') }}" method="post">
                        @csrf
                        <div class="mb-3">
                            <h3>Visi</h3>
                            <textarea name="visi" id="visi" class="form-control">{{ $visi->val }}</textarea>
                        </div>
                        <div class="mb-3">
                            <h3>Misi</h3>
                            <textarea name="misi" id="misi" class="form-control">{{ $misi->val }}</textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Kirim</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <h3>Preview</h3>
                    <hr />
                    <div class="mb-4">
                        <div class="mb-2">
                            <h5 class="fw-bold mb-3 text-uppercase" style="color: #0A4C7F;">Visi</h5>
                            <div class="rounded" style="background-color: #E9974B;width:80px;height:6px;"></div>
                        </div>
                        <p>{!! $visi->val !!}</p>
                    </div>
                    <div class="">
                        <div class="mb-2">
                            <h5 class="fw-bold mb-3 text-uppercase" style="color: #0A4C7F;">Misi</h5>
                            <div class="rounded" style="background-color: #E9974B;width:80px;height:6px;"></div>
                        </div>
                        <p>{!! $misi->val !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    @php
        initialAction();
    @endphp
    <script src="https://cdn.ckeditor.com/ckeditor5/35.3.1/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#visi'))
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#misi'))
            .catch(error => {
                console.error(error);
            });
    </script>
@endsection
