<?php

use Illuminate\Support\Facades\Route;
use Modules\UserManagement\Http\Controllers\RoleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('user-management')->group(function () {
    Route::get('/', 'UserManagementController@index');

    Route::resource('role', RoleController::class);
});
