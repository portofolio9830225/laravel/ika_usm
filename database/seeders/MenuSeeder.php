<?php

namespace Database\Seeders;

use App\Models\Menu;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = [
            [
                'name_menu'     => 'Dashboard',
                'link'          => 'home',
                'is_parent'     => true,
                'parent_icon'   => 'fa fa-home',
                'permission_key' => 'home'
            ],
            [
                'name_menu'     => 'User Management',
                'link'          => '#',
                'is_parent'     => true,
                'parent_icon'   => 'fa fa-users',
                'sub_menu'      => [
                    [
                        'name_menu'     => 'Role',
                        'link'          => 'user-management/role',
                        'is_parent'     => false,
                        'permission_key' => 'role',
                    ],
                    [
                        'name_menu'     => 'User List',
                        'link'          => 'user-management/user-list',
                        'is_parent'     => false,
                        'permission_key' => 'user-list',
                    ],
                ]
            ],
        ];

        foreach ($menu as $item) {
            $arr = Arr::only($item, ['name_menu', 'link', 'is_parent', 'parent_icon']);
            if (isset($item['permission_key'])) {
                $arr['permission_key'] = $item['permission_key'];
            }
            $menu = Menu::create($arr);

            if (isset($item['sub_menu'])) {
                foreach ($item['sub_menu'] as $val) {
                    $arr = Arr::only($val, ['name_menu', 'link', 'is_parent', 'permission_key']);
                    $arr['parent_id'] = $menu->id;
                    Menu::create($arr);
                }
            }
        }
    }
}
