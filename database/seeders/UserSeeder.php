<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $role = Role::create(['name' => 'superadmin']);

        $salt = uniqid(mt_rand(), true);
        User::create([
            'name'      => 'superadmin',
            'email'     => 'superadmin@gmail.com',
            'salt'      => $salt,
            'password'  => Hash::make($salt . 'password'),
            'role_id'   => 1,
            'level'     => 'superadmin',
        ]);
    }
}
