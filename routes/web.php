<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\FrontEndController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FrontEndController::class, 'index']);
Route::get('kegiatan-ika', [FrontEndController::class, 'kegiatan_ika']);
Route::get('tips-karier', [FrontEndController::class, 'tips_karier']);
Route::get('career-class/{slug}', [FrontEndController::class, 'career_class_detail']);
Route::get('struktur-organisasi', [FrontEndController::class, 'struktur_organisasi']);
Route::get('profile', [FrontEndController::class, 'profil_directori_staff']);
Route::get('kartu-tanda-alumni', [FrontEndController::class, 'kartu_tanda_alumni']);
Route::get('lowongan-kerja', [FrontEndController::class, 'lowongan_kerja']);
Route::get('lowongan-kerja/{slug}', [FrontEndController::class, 'lowongan_kerja_detail']);
Route::get('lowongan-magang', [FrontEndController::class, 'lowongan_magang']);
Route::get('alumni-unggul', [FrontEndController::class, 'alumni_unggul']);
Route::get('news-alumni', [FrontEndController::class, 'news_alumni']);
Route::get('contact-us', [FrontEndController::class, 'contact_us']);
Route::get('layanan-alumni', [FrontEndController::class, 'layanan_alumni']);
Route::get('layanan-karier', [FrontEndController::class, 'layanan_karier']);

Route::get('login', [AuthController::class, 'index']);
Route::post('login', [AuthController::class, 'auth']);
Route::post('logout', [AuthController::class, 'logout']);

Route::get('artisan', function () {
    Artisan::call('migrate:fresh --seed');
    return 'DONE';
});
